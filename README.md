# Quaderno Minimo per Abiword

### (it): Un manuale minimo in italiano per l'uso del wordprocessor Abiword
### (en): A minimal manual in Italian for the use of the wordprocessor Abiword

**Indice del contenuto**

* AbiWord: Come e perché
* Barre e Menù
* Creare e modificare un documento
* Formattazione diretta del testo
* Formattazione attraverso gli stili
* Inserire immagini
* Inserire e modificare una tabella	
* Intestazioni, piè di pagine e note
* Controllo ortografico
* Stampare un documento
* Salvare un documento
* Altre funzioni utili